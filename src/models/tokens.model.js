'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const token = sequelizeClient.define('tokens', {
		hash: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		user_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		expires_at: {
			type: Sequelize.DATE,
			allowNull: false,
			defaultValue: Date.now() + 86400000
		},
		used_at: {
			type: Sequelize.DATE,
			allowNull: false,
			defaultValue: -1
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'token_user_key',
				method: 'BTREE',
				fields: ['user_id']
			}
		],
		classMethods: {
			associate (models) {
				token.belongsTo(models.users, {foreignKey: 'user_id'});
			}
		}
	});

	return token;
};
