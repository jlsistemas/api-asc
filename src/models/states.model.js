'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const states = sequelizeClient.define('states', {
		state_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		country_id: {
			type: Sequelize.UUID,
			allowNull: false,
			model: 'Country',
			key: 'country_id'
		},
		name: {
			type: Sequelize.STRING(80),
			unique: true,
			allowNull: false
		},
		abbreviation: {
			type: Sequelize.STRING(10)
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'state_country_key',
				method: 'BTREE',
				fields: ['country_id']
			},
			{
				name: 'state_status_key',
				method: 'BTREE',
				fields: ['status']
			}
		],
		classMethods: {
			associate (models) {
				states.belongsTo(models.countries, {foreignKey: 'country_id'});
			}
		}
	});

	return states;
};
