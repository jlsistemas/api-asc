'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const schoolYear = sequelizeClient.define('school_year', {
		school_year_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		country_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		school_year_code: {
			type: Sequelize.STRING(10),
			allowNull: false,
			unique: true
		},
		name: {
			type: Sequelize.STRING,
			unique: true,
			allowNull: false
		},
		date_start: {
			type: Sequelize.DATE,
			allowNull: false
		},
		date_end: {
			type: Sequelize.DATE,
			allowNull: false
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'school_year_country_id_key',
				method: 'BTREE',
				fields: ['country_id']
			},
			{
				name: 'school_year_code_key',
				method: 'BTREE',
				fields: ['school_year_code']
			},
			{
				name: 'school_year_status_key',
				method: 'BTREE',
				fields: ['status']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				schoolYear.belongsTo(models.countries, {
					foreignKey: 'country_id'
				});
			}
		}
	});

	return schoolYear;
};
