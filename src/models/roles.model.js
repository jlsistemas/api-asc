'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const role = sequelizeClient.define('roles', {
		role_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		parent_id: {
			type: Sequelize.UUID
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
			unique: true
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'roles_status_key',
				method: 'BTREE',
				fields: ['status']
			},
			{
				name: 'roles_parent_key',
				method: 'BTREE',
				fields: ['parent_id']
			}
		],
		classMethods: {
			associate (models) {
				role.belongsToMany(models.users, {
					through: models.user_roles,
					foreignKey: 'role_id'
				});
				role.hasMany(models.user_roles, {
					foreignKey: 'role_id'
				});
				role.belongsToMany(models.permissions, {
					through: models.role_permissions,
					foreignKey: 'role_id'
				});
				role.hasMany(models.role_permissions, {
					foreignKey: 'role_id'
				});
			}
		}
	});

	return role;
};
