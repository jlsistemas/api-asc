'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const orders = sequelizeClient.define('orders', {
		order_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		order_id_gea: {
			type: Sequelize.BIGINT,
			unique: true
		},
		country_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		buyer_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		payment_method: {
			type: Sequelize.STRING
		},
		date: {
			type: Sequelize.DATE,
			allowNull: false
		},
		reference: {
			type: Sequelize.STRING,
			allowNull: false
		},
		total_amount: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false
		},
		currency_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		price_list_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'order_country_id_key',
				method: 'BTREE',
				fields: ['country_id']
			},
			{
				name: 'status_key',
				method: 'BTREE',
				fields: ['status']
			},
			{
				name: 'order_id_gea_key',
				method: 'BTREE',
				fields: ['order_id_gea']
			},
			{
				name: 'buyer_id_key',
				method: 'BTREE',
				fields: ['buyer_id']
			},
			{
				name: 'price_list_id_key',
				method: 'BTREE',
				fields: ['price_list_id']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				orders.belongsTo(models.countries, {
					foreignKey: 'country_id'
				});
				orders.belongsTo(models.users, {
					foreignKey: 'user_id'
				});
				orders.belongsTo(models.currencies, {
					foreignKey: 'currency_id'
				});
				orders.belongsTo(models.price_list, {
					foreignKey: 'price_list_id'
				});
				orders.hasMany(models.order_detail, {
					foreignKey: 'order_id'
				});
			}
		}
	});

	return orders;
};
