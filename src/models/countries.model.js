'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const country = sequelizeClient.define('countries', {
		country_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING,
			unique: true,
			allowNull: false
		},
		code: {
			type: Sequelize.STRING(3),
			allowNull: false
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'country_code_key',
				method: 'BTREE',
				fields: ['code']
			},
			{
				name: 'country_status_key',
				method: 'BTREE',
				fields: ['status']
			}
		],
		classMethods: {
			associate (models) {
				country.hasMany(models.states, {foreignKey: 'country_id'});
				country.hasMany(models.roles, {foreignKey: 'country_id'});
			}
		}
	});

	return country;
};
