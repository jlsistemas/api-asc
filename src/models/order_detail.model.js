'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const orderDetail = sequelizeClient.define('order_detail', {
		order_detail_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		order_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		item_number: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		student_id: {
			type: Sequelize.UUID
		},
		product_level_price_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		description: {
			type: Sequelize.STRING,
			allowNull: false
		},
		quantity: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		price: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false
		},
		discount_percentage: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false,
			defaultValue: 0
		},
		charges: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false,
			defaultValue: 0
		},
		charges_tax: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false,
			defaultValue: 0
		},
		raw_data: {
			type: Sequelize.JSONB
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'order_id_key',
				method: 'BTREE',
				fields: ['order_id']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				orderDetail.belongsTo(models.orders, {
					foreignKey: 'order_id'
				});
				orderDetail.belongsTo(models.product_level_price, {
					foreignKey: 'product_level_price_id'
				});
			}
		}
	});

	return orderDetail;
};
