'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const students = sequelizeClient.define('students', {
		student_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		user_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		school_year_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		id_asc: {
			type: Sequelize.INTEGER,
			allowNull: false,
			unique: true
		},
		id_gea: {
			type: Sequelize.INTEGER,
			allowNull: false,
			unique: true
		},
		school_level_year_id: {
			type: Sequelize.UUID
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'student_status_key',
				method: 'BTREE',
				fields: ['status']
			},
			{
				name: 'student_id_gea_key',
				method: 'BTREE',
				fields: ['id_gea']
			},
			{
				name: 'student_id_asc_key',
				method: 'BTREE',
				fields: ['id_asc']
			},
			{
				name: 'school_year_key',
				method: 'BTREE',
				fields: ['school_year_id']
			},
			{
				name: 'student_user_id_key',
				method: 'BTREE',
				fields: ['user_id']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				students.belongsTo(models.users, {
					foreignKey: 'user_id'
				});
				students.belongsTo(models.school_year, {
					foreignKey: 'school_year_id'
				});
			}
		}
	});

	return students;
};
