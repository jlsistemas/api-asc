'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const userRoles = sequelizeClient.define('user_roles', {
		user_role_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		user_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		role_id: {
			type: Sequelize.UUID,
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'user_role_user_key',
				method: 'BTREE',
				fields: ['user_id']
			},
			{
				name: 'user_role_role_key',
				method: 'BTREE',
				fields: ['role_id']
			}
		],
		classMethods: {
			associate (models) {
				userRoles.belongsTo(models.users, {
					foreignKey: 'user_id'
				});
				userRoles.belongsTo(models.roles, {
					foreignKey: 'role_id'
				});
			}
		}
	});

	return userRoles;
};
