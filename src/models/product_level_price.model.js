'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const productLevelPrice = sequelizeClient.define('product_level_price', {
		product_level_price_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		product_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		price_list_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		school_year_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		price: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false
		},
		tax_percentage: {
			type: Sequelize.DECIMAL(12, 4),
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'product_id_key',
				method: 'BTREE',
				fields: ['product_id']
			},
			{
				name: 'product_level_price_price_list_id_key',
				method: 'BTREE',
				fields: ['price_list_id']
			},
			{
				name: 'school_year_id_key',
				method: 'BTREE',
				fields: ['school_year_id']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				productLevelPrice.belongsTo(models.products, {
					foreignKey: 'product_id'
				});
				productLevelPrice.belongsTo(models.price_list, {
					foreignKey: 'price_list_id'
				});
				productLevelPrice.belongsTo(models.school_year, {
					foreignKey: 'school_year_id'
				});
			}
		}
	});

	return productLevelPrice;
};
