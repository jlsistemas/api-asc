'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const users = sequelizeClient.define('users', {
		user_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		id_gea: {
			type: Sequelize.INTEGER,
			unique: true
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false
		},
		last_name: {
			type: Sequelize.STRING(100),
			allowNull: false
		},
		second_last_name: {
			type: Sequelize.STRING(100),
			allowNull: true
		},
		username: {
			type: Sequelize.STRING(80),
			allowNull: true,
			unique: true
		},
		email: {
			type: Sequelize.STRING,
			allowNull: true
		},
		password: {
			type: Sequelize.STRING,
			allowNull: true
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'user_status_key',
				method: 'BTREE',
				fields: ['status']
			},
			{
				name: 'id_gea_key',
				method: 'BTREE',
				fields: ['id_gea']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				users.belongsToMany(models.roles, {
					through: models.user_roles,
					foreignKey: 'user_id'
				}),
				users.hasMany(models.tokens, {
					foreignKey: 'user_id'
				});
				users.hasMany(models.students, {
					foreignKey: 'user_id'
				});
			}
		}
	});

	return users;
};
