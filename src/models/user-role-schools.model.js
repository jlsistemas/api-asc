'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const userRoleSchools = sequelizeClient.define('user_role_schools', {
		user_role_school_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		user_role_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		school_id: {
			type: Sequelize.UUID,
			allowNull: false
		}
	}, {
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
			}
		}
	});

	return userRoleSchools;
};
