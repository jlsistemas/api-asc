'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const schools = sequelizeClient.define('schools', {
		school_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false
		},
		country_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'school_status_key',
				method: 'BTREE',
				fields: ['status']
			}
		],
		classMethods: {
			associate (models) {
				schools.belongsTo(models.countries, {
					foreignKey: 'country_id'
				});
			}
		}
	});

	return schools;
};
