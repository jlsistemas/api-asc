'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const apiKeys = sequelizeClient.define('api_keys', {
		api_keys_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUID1,
			primaryKey: true,
			allowNull: false
		},
		client: {
			type: Sequelize.STRING(100),
			allowNull: false,
			unique: true
		},
		code: {
			type: Sequelize.STRING(6),
			allowNull: false,
			unique: true
		},
		key: {
			type: Sequelize.STRING(50),
			allowNull: false,
			unique: true
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		}
	}, {
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
			}
		}
	});

	return apiKeys;
};
