'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const logs = sequelizeClient.define('logs', {
		log_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		endpoint: {
			type: Sequelize.STRING,
			allowNull: false
		},
		date: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.NOW,
			allowNull: false
		},
		duration: {
			type: Sequelize.BIGINT,
			allowNull: false
		},
		ip: {
			type: Sequelize.STRING(40),
			allowNull: false
		},
		user_id: {
			type: Sequelize.UUID
		},
		app_code: {
			type: Sequelize.STRING(5)
		},
		method: {
			type: Sequelize.STRING
		},
		payload: {
			type: Sequelize.JSONB
		}
	}, {
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
			}
		}
	});

	return logs;
};
