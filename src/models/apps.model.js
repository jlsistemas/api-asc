'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const apps = sequelizeClient.define('apps', {
		app_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
			unique: true
		},
		code: {
			type: Sequelize.STRING(5),
			allowNull: false,
			unique: true
		},
		url: {
			type: Sequelize.STRING,
			allowNull: false,
			unique: true
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		}
	}, {
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
			}
		}
	});

	return apps;
};
