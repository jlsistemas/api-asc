'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const userPermissions = sequelizeClient.define('user_permissions', {
		user_permission_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		user_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		permission_id: {
			type: Sequelize.UUID,
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'user_permission_user_key',
				method: 'BTREE',
				fields: ['user_id']
			},
			{
				name: 'user_permission_permission_key',
				method: 'BTREE',
				fields: ['permission_id']
			},
			{
				unique: true,
				fields: ['user_id', 'permission_id']
			}
		]
	});

	return userPermissions;
};
