'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const rolePermissions = sequelizeClient.define('role_permissions', {
		role_permission_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		role_id: {
			type: Sequelize.UUID,
			allowNull: false
		},
		permission_id: {
			type: Sequelize.UUID,
			allowNull: false
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'role_permission_role_key',
				method: 'BTREE',
				fields: ['role_id']
			},
			{
				name: 'role_permission_permission_key',
				method: 'BTREE',
				fields: ['permission_id']
			},
			{
				unique: true,
				fields: ['role_id', 'permission_id']
			}
		],
		classMethods: {
			associate (models) {
				rolePermissions.belongsTo(models.roles, {
					foreignKey: 'role_id'
				});
				rolePermissions.belongsTo(models.permissions, {
					foreignKey: 'permission_id'
				});
			}
		}
	});

	return rolePermissions;
};
