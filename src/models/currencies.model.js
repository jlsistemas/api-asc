'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const currencies = sequelizeClient.define('currencies', {
		currency_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING(50),
			allowNull: false,
			unique: true
		},
		code: {
			type: Sequelize.STRING(3),
			allowNull: false,
			unique: true
		},
		show_before: {
			type: Sequelize.STRING(10)
		},
		show_after: {
			type: Sequelize.STRING(10)
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'currency_code_key',
				method: 'BTREE',
				fields: ['code']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				currencies.belongsTo(models.countries, {
					foreignKey: 'country_id'
				});
			}
		}
	});

	return currencies;
};
