'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const products = sequelizeClient.define('products', {
		product_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		product_code: {
			type: Sequelize.STRING(20),
			allowNull: false,
			unique: true
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false,
			unique: true
		},
		ean: {
			type: Sequelize.STRING(100),
			unique: true
		},
		sku: {
			type: Sequelize.STRING(100),
			unique: true,
			allowNull: false
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'product_status_key',
				method: 'BTREE',
				fields: ['status']
			},
			{
				name: 'product_code_key',
				method: 'BTREE',
				fields: ['product_code']
			},
			{
				name: 'ean_key',
				method: 'BTREE',
				fields: ['ean']
			},
			{
				name: 'sku_key',
				method: 'BTREE',
				fields: ['sku']
			}
		],
		classMethods: {
			associate (models) {
				// Define associations here
				// See http://docs.sequelizejs.com/en/latest/docs/associations/
				products.hasMany(models.product_level_price, {
					foreignKey: 'product_id'
				});
			}
		}
	});

	return products;
};
