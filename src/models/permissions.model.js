'use strict';

// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
	const sequelizeClient = app.get('sequelizeClient');
	const permission = sequelizeClient.define('permissions', {
		permission_id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV1,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
			unique: true
		},
		status: {
			type: Sequelize.ENUM,
			values: ['active', 'inactive'],
			defaultValue: 'active',
			allowNull: false
		},
		status_changed_date: {
			type: Sequelize.DATE
		}
	}, {
		underscored: true,
		timestamps: true,
		createdAt: 'created_date',
		updatedAt: 'updated_date',
		freezeTableName: true,
		indexes: [
			{
				name: 'permissions_status_key',
				method: 'BTREE',
				fields: ['status']
			}
		],
		classMethods: {
			associate (models) {
				permission.belongsToMany(models.roles, {
					through: models.role_permissions,
					foreignKey: 'permission_id'
				});
				permission.hasMany(models.role_permissions, {
					foreignKey: 'permission_id'
				});
			}
		}
	});

	return permission;
};
