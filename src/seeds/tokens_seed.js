module.exports = [
	{
		hash: '52b2798e-151f-4889-8953-8710faf47aef',
		user_id: '22d5362c-efdc-11e6-bc64-92361f002671'
	},
	{
		hash: '3f5d8ec5-4b2a-4f73-a250-76dd3bdab43a',
		user_id: '22d5362c-efdc-11e6-bc64-92361f002671',
		expires_at: Date.now() - 86400000
	},
	{
		hash: '87117cb3-46bb-41b3-9217-ab9ac5093869',
		user_id: '22d5362c-efdc-11e6-bc64-92361f002671',
		used_at: Date.now() - 86400000
	}
];
