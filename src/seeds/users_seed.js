module.exports = [
	{
		user_id: '22d5362c-efdc-11e6-bc64-92361f002671',
		username: 'jlpellicerm',
		email: 'jlpellicerm@gmail.com',
		password: '$2a$10$qPDpLS1YZYlzHx2B3yYDcu7xCIJ63sVC34v6ttS91aFsLtz758n46',
		name: 'José Luis',
		last_name: 'Pellicer',
		second_last_name: 'Monterrubio'
	},
	{
		user_id: '51ac40c0-7965-456e-90f1-2f9123cfc5d6',
		username: 'jlpellicerm2',
		email: 'jlpellicerm@gmail.com',
		password: '$2a$10$qPDpLS1YZYlzHx2B3yYDcu7xCIJ63sVC34v6ttS91aFsLtz758n46',
		name: 'José Luis2',
		last_name: 'Pellicer2',
		second_last_name: 'Monterrubio2'
	},
	{
		user_id: '5abec6e0-efda-11e6-bfc0-4979bfeeb47e',
		username: 'transistor',
		email: 'jlsistemas@aol.com',
		password: '$2a$10$4hiEaSCRvlgIz4Ov6ffrl.yZiC8B8zIJKyG5tUD8NN31zKyiKlcjC',
		status: 'active',
		name: 'JL',
		last_name: 'Pellicer',
		second_last_name: 'M'
	},
	{
		user_id: '4eca2bc0-ffb3-11e6-b0c4-6f14ee59651a',
		username: 'pepito',
		email: 'pierbover11@gmail.com',
		password: '$2a$10$asZ3g03MNgZMbnnMkhI0AOH.o73VvPqF5YvBt2p6EViUOS9/nbKvW',
		status: 'active',
		name: 'Pier',
		last_name: 'Bover'
	}
];
