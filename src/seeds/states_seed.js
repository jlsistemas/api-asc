module.exports = [
	{
		state_id: '76ceff98-f003-11e6-bc64-92361f002671',
		name: 'Aguascalientes',
		abbreviation: 'Ags.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075cf0e-f003-11e6-bc64-92361f002671',
		name: 'Baja California',
		abbreviation: 'B.C.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075d094-f003-11e6-bc64-92361f002671',
		name: 'Baja California Sur',
		abbreviation: 'B.C.S.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075d242-f003-11e6-bc64-92361f002671',
		name: 'Campeche',
		abbreviation: 'Camp.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075d3f0-f003-11e6-bc64-92361f002671',
		name: 'Chiapas',
		abbreviation: 'Chis.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075d706-f003-11e6-bc64-92361f002671',
		name: 'Chihuahua',
		abbreviation: 'Chih.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075da6c-f003-11e6-bc64-92361f002671',
		name: 'Coahuila',
		abbreviation: 'Coah.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075db84-f003-11e6-bc64-92361f002671',
		name: 'Colima',
		abbreviation: 'Col.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075dc4c-f003-11e6-bc64-92361f002671',
		name: 'Ciudad de México',
		abbreviation: 'CDMX',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075dd14-f003-11e6-bc64-92361f002671',
		name: 'Durango',
		abbreviation: 'Dgo.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075dde6-f003-11e6-bc64-92361f002671',
		name: 'Guanajuato',
		abbreviation: 'Gto.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075deb8-f003-11e6-bc64-92361f002671',
		name: 'Guerrero',
		abbreviation: 'Gro.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075e5f2-f003-11e6-bc64-92361f002671',
		name: 'Hidalgo',
		abbreviation: 'Hgo.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075e70a-f003-11e6-bc64-92361f002671',
		name: 'Jalisco',
		abbreviation: 'Jal.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075e7dc-f003-11e6-bc64-92361f002671',
		name: 'México',
		abbreviation: 'Méx.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075e8ae-f003-11e6-bc64-92361f002671',
		name: 'Michoacán',
		abbreviation: 'Mich.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075e96c-f003-11e6-bc64-92361f002671',
		name: 'Morelos',
		abbreviation: 'Mor.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075ea2a-f003-11e6-bc64-92361f002671',
		name: 'Nayarit',
		abbreviation: 'Nay.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075eaf2-f003-11e6-bc64-92361f002671',
		name: 'Nuevo León',
		abbreviation: 'N.L.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f0e2-f003-11e6-bc64-92361f002671',
		name: 'Oaxaca',
		abbreviation: 'Oax.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f1d2-f003-11e6-bc64-92361f002671',
		name: 'Puebla',
		abbreviation: 'Pue.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f29a-f003-11e6-bc64-92361f002671',
		name: 'Querétaro',
		abbreviation: 'Qro.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f376-f003-11e6-bc64-92361f002671',
		name: 'Quintana Roo',
		abbreviation: 'Q.R.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f448-f003-11e6-bc64-92361f002671',
		name: 'San Luis Potosí',
		abbreviation: 'S.L.P.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f510-f003-11e6-bc64-92361f002671',
		name: 'Sinaloa',
		abbreviation: 'Sin.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075f5d8-f003-11e6-bc64-92361f002671',
		name: 'Sonora',
		abbreviation: 'Son.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075fcb8-f003-11e6-bc64-92361f002671',
		name: 'Tabasco',
		abbreviation: 'Tab.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075fda8-f003-11e6-bc64-92361f002671',
		name: 'Tamaulipas',
		abbreviation: 'Tamps.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075fe84-f003-11e6-bc64-92361f002671',
		name: 'Tlaxcala',
		abbreviation: 'Tlax.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '8075ff6a-f003-11e6-bc64-92361f002671',
		name: 'Veracruz',
		abbreviation: 'Ver.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '80760032-f003-11e6-bc64-92361f002671',
		name: 'Yucatán',
		abbreviation: 'Yuc.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		state_id: '807600f0-f003-11e6-bc64-92361f002671',
		name: 'Zacatecas',
		abbreviation: 'Zac.',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	}
];
