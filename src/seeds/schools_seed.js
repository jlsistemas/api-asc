module.exports = [
	{
		school_id: 'a03ca336-f002-11e6-bc64-92361f002671',
		name: 'Colegio de pruebas',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		school_id: 'ebc509b0-f002-11e6-bc64-92361f002671',
		name: 'Colegio Alexandre',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671'
	},
	{
		school_id: 'e1614592-f002-11e6-bc64-92361f002671',
		name: 'Universidad Patito',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671',
		status: 'inactive'
	},
	{
		school_id: 'd97e6b18-f483-11e6-bc64-92361f002671',
		name: 'Escuela Panamá',
		country_id: '5f378c58-f001-11e6-bc64-92361f002671',
	}
];
