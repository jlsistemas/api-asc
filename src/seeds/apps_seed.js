module.exports = [
	{
		app_id: '402606be-3053-4934-8486-cf6de163c4d1',
		name: 'Pagos',
		code: 'PAG',
		url: 'https://pagos.ascc.me'
	},
	{
		app_id: '75e1ff71-7768-46cf-9e9e-b5d0a2fb7c1c',
		name: 'Censo',
		code: 'CEN',
		url: 'https://censo.ascc.me'
	}
];
