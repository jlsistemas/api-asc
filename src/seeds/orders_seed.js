module.exports = [
	{
		order_id: '299e681e-e29e-42d5-9095-b341ecd9048e',
		order_id_gea: '7823',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671',
		buyer_id: '51ac40c0-7965-456e-90f1-2f9123cfc5d6',
		payment_method: 'transferencia',
		date: '2017-05-11 12:19:57.529-05',
		reference: '5335367960',
		total_amount: 8100,
		currency_id: 'f610dc19-33be-4aaf-9fbd-404f8c63ea10',
		price_list_id: '9a172b42-72a7-4221-bb10-31bfdb093a5a',
		status: 'active'
	},
	{
		order_id: '4c8ea3e0-67ee-4bee-9526-43e51571a9cf',
		order_id_gea: '1937',
		country_id: '48d4131e-f001-11e6-bc64-92361f002671',
		buyer_id: '5abec6e0-efda-11e6-bfc0-4979bfeeb47e',
		payment_method: 'tarjeta de crédito',
		date: '2017-05-12 12:19:57.529-05',
		reference: '67960533537',
		total_amount: 4050,
		currency_id: 'f610dc19-33be-4aaf-9fbd-404f8c63ea10',
		price_list_id: '9a172b42-72a7-4221-bb10-31bfdb093a5a',
		status: 'active'
	}
];
