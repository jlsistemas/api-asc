module.exports = [
	{
		product_id: 'dbb9af23-1252-440a-b542-b6d7f20b6bf5',
		product_code: 'PEP16114',
		name: 'Primaria en forma 1ro México',
		sku: 'PEP16114',
	},
	{
		product_id: '6c8701cf-6bd8-453b-b8c7-fafaebe4afdd',
		product_code: 'PEP26114',
		name: 'Primaria en forma 2do México',
		sku: 'PEP26114',
	},
	{
		product_id: '104512e8-15e6-4e2a-86c4-e628c542f583',
		product_code: 'PEP36114',
		name: 'Primaria en forma 3ro México',
		sku: 'PEP36114',
	},
	{
		product_id: '52b465d5-3ac5-4c43-9dc5-b1b5a714a390',
		product_code: 'PEP46114',
		name: 'Primaria en forma 4to México',
		sku: 'PEP46114',
	},
	{
		product_id: 'c8968706-a9fc-4d08-a365-cda6bd670893',
		product_code: 'PEP56114',
		name: 'Primaria en forma 5to México',
		sku: 'PEP56114',
	},
	{
		product_id: '0ec73695-db7f-4a16-a0a4-3e3413ec693d',
		product_code: 'PEP66114',
		name: 'Primaria en forma 6to México',
		sku: 'PEP66114',
	}
];
