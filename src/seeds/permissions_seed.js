module.exports = [
	{
		permission_id: 'ca025900-f001-11e6-bc64-92361f002671',
		name: 'countries_find'
	},
	{
		permission_id: 'ee627780-f001-11e6-bc64-92361f002671',
		name: 'countries_get'
	},
	{
		permission_id: '028cc63e-f002-11e6-bc64-92361f002671',
		name: 'countries_post'
	},
	{
		permission_id: '15924876-f002-11e6-bc64-92361f002671',
		name: 'countries_remove'
	},
	{
		permission_id: '2f40e4b2-f002-11e6-bc64-92361f002671',
		name: 'countries_create'
	},
	{
		permission_id: '407967ae-f002-11e6-bc64-92361f002671',
		name: 'countries_patch'
	},
	{
		permission_id: '55299dfe-f002-11e6-bc64-92361f002671',
		name: 'countries_update'
	}
];
