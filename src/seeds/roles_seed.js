module.exports = [
	{
		role_id: '3d7f9f70-f000-11e6-bc64-92361f002671',
		name: 'admin'
	},
	{
		role_id: '94ab45e2-f000-11e6-bc64-92361f002671',
		name: 'student'
	},
	{
		role_id: 'a3688c7a-f000-11e6-bc64-92361f002671',
		name: 'teacher',
		status: 'active'
	},
	{
		role_id: '20349f70-b1aa-4316-8f86-e50bc721ca29',
		name: 'admin-school',
		status: 'inactive'
	},
	{
		role_id: '876be94c-bded-4620-b59a-9f7bb5354b9d',
		name: 'admin-gea',
		status: 'inactive'
	},
	{
		role_id: '74185015-ec95-4049-9acb-6322b49e9b62',
		name: 'rol de prueba',
		status: 'inactive'
	}
];
