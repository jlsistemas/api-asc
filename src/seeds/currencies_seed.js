module.exports = [
	{
		currency_id: 'f610dc19-33be-4aaf-9fbd-404f8c63ea10',
		name: 'Pesos Mexicanos',
		code: 'MXN',
		show_before: '$',
		show_after: 'MXN'
	},
	{
		currency_id: '5ae8eb8f-77af-4b83-81fd-4fded01e8649',
		name: 'Dólar americano',
		code: 'USD',
		show_before: '$',
		show_after: 'USD'
	}
];
