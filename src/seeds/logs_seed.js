module.exports = [
	{
		log_id: '34f06c23-b921-492e-ab33-d7abfc338561',
		endpoint: 'pagos/tarjeta_credito',
		date: '2017-03-10 12:06:57.529-05',
		duration: '234',
		ip: '192.168.100.128/25',
		payload: {'apple':'red','banana':'yellow','vegetables':{'artichoke':'yumm','broccoli':'green'}}
	},
	{
		log_id: '05f61b5c-c847-4efb-8951-33f7e4e41118',
		endpoint: 'pagos/tarjeta_credito',
		date: '2017-03-10 12:19:57.529-05',
		duration: '212',
		ip: '192.168.10.12/25',
		payload: {'apple':'green','banana':'brown','vegetables':{'artichoke':'yuck','broccoli':'delish'}}
	},
];
