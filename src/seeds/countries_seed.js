module.exports = [
	{
		country_id: '48d4131e-f001-11e6-bc64-92361f002671',
		name: 'México',
		code: 'mx'
	},
	{
		country_id: '5f378c58-f001-11e6-bc64-92361f002671',
		name: 'Panamá',
		code: 'pa'
	},
	{
		country_id: '74f75924-f001-11e6-bc64-92361f002671',
		name: 'España',
		code: 'es',
		status: 'inactive'
	}
];
