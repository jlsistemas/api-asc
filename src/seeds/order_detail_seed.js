module.exports = [
	{
		order_detail_id: '66f26fad-93c4-4fa2-b269-7b68e2ee7b5f',
		order_id: '299e681e-e29e-42d5-9095-b341ecd9048e',
		item_number: 1,
		student_id: '48d4131e-f001-11e6-bc64-92361f002671',
		product_level_price_id: '2fde947c-3dde-4cc6-bfab-057acd966294',
		description: 'PRIMARIA EN FORMA 1ro México',
		quantity: 1,
		price: 4050,
		discount_percentage: 0,
		charges: 0,
		charges_tax: 0
	},
	{
		order_detail_id: '713c9f6c-321d-4bf5-9fe6-fad17669d36f',
		order_id: '299e681e-e29e-42d5-9095-b341ecd9048e',
		item_number: 2,
		student_id: '51ac40c0-7965-456e-90f1-2f9123cfc5d6',
		product_level_price_id: 'aa28ae61-182e-4de4-9dcf-cc3c0835e6ac',
		description: 'PRIMARIA EN FORMA 2do México',
		quantity: 1,
		price: 4050,
		discount_percentage: 0,
		charges: 0,
		charges_tax: 0
	},
	{
		order_detail_id: 'fc36cb27-016a-49ca-ad78-e833842004bf',
		order_id: '4c8ea3e0-67ee-4bee-9526-43e51571a9cf',
		item_number: 1,
		student_id: '22d5362c-efdc-11e6-bc64-92361f002671',
		product_level_price_id: 'd6a8ffb0-3ddc-407b-964c-cd323a210fb0',
		description: 'PRIMARIA EN FORMA 3ro México',
		quantity: 1,
		price: 4050,
		discount_percentage: 0,
		charges: 0,
		charges_tax: 0
	},
];
