'use strict';
const logger = require('winston');
const sequence = require('promise-sequence');
const Sequelize = require('sequelize');

module.exports = function () {
	const app = this;
	const connectionString = app.get('postgres');
	const sequelize = new Sequelize(connectionString, {
		dialect: 'postgres',
		logging: false,
		define: {
			freezeTableName: true
		}
	});
	const oldSetup = app.setup;

	app.set('sequelizeClient', sequelize);

	app.setup = function (...args) {
		const result = oldSetup.apply(this, args);

		// Set up data relationships
		const models = sequelize.models;
		Object.keys(models).forEach((name) => {
			if ('associate' in models[name]) {
				models[name].associate(models);
			}
		});

		// Sync to the database
		const seeds = [];
		sequelize.sync({force: true}).then(()=>{
			logger.info('Seeding started');
			Object.keys(models)
			.map((name) => models[name])
			.forEach((model) => seeds.push(seedTheModel(model)));
			return sequence(seeds);
		}).then((results)=>{
			logger.info('Seeding done');
		}).catch((error)=>{
			logger.error('Seeding error ', error.toString());
		});

		function seedTheModel (model) {
			return model.bulkCreate(require('./seeds/' + model.name + '_seed.js'));
		}

		return result;
	};
};
