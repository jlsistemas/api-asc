const errors = require('feathers-errors');
exports.checkRole = function (options) {
	return function (hook) {
		// en user ponemos los datos para fácil acceso
		console.log(hook);
		const user = hook.params.user.dataValues;
		return new Promise((resolve, reject) => {
			// path y method se usan en combinación para determinar el permiso
			let path = hook.path;
			let method = hook.method;
			if (path == undefined || path == '' || method == undefined || method == '') {
				let error = 'Error, path y method no deben estar vacíos';
				console.log(error);
				reject(new errors.BadRequest(error));
			}
			// inicializar sequelize
			const sequelize = hook.app.get('sequelizeClient');
			// importamos los models necesarios
			const User = sequelize.models.users;
			const Roles = sequelize.models.roles;
			// query para encontrar los permisos asociados a los roles de el usuario
			return Roles.findAll({
				attributes: ['role_id'],
				where: {
					status: 'active'
				},
				include: [
					{
						model: sequelize.models.users,
						attributes: ['user_id'],
						where: {
							user_id: user.user_id,
							status: 'active'
						}
					},
					{
						model: sequelize.models.permissions,
						attributes: ['permission_id', 'name'],
						where: {
							name: path + '_' + method,
							status: 'active'
						}
					}
				]
			}).then((user) => {
				// si encontramos un usuario y tiene al menos un permiso
				if (user && user.length > 0) {
					resolve(hook);
				}
				// si no se encontró nada, no tiene permiso
				return reject(new errors.Forbidden('Access denied'));
			}).catch((error)=>{
				console.log(error.toString());
				reject(error);
			});
		});
	};
};
