'use strict';

const errors = require('feathers-errors');
const handlebars = require('handlebars');
const layouts = require('handlebars-layouts');
const fs = require('fs');
const logger = require('winston');
// const AWS = require('./aws');
// Handlebars config
const hbsTmplDir = 'src/templates/email';
handlebars.registerHelper(layouts(handlebars));
handlebars.registerPartial('layout', fs.readFileSync(`${hbsTmplDir}/layout.hbs`, 'utf8'));
// SendGrid init
let SendGrid = require('sendgrid').mail;
let sg = require('sendgrid')(process.env.MAILER_SENDGRID_API_KEY);
/*
	config:
		data:
		from:
			email
			name
		to
		subject
		plain
		template
*/
exports.send = function (options) {
	return function (hook) {
		return new Promise((resolve, reject) => {
			console.log('SEND EMAIL');
			let config = hook.result;
			let data = config.data || {};
			data.app_url = process.env.APP_URL;
			let fromEmail, fromName, toEmail;

			if (config.from) {
				fromEmail = config.from.email;
				fromName = config.from.name;
			} else {
				fromEmail = process.env.MAILER_SENDGRID_FROM_EMAIL;
				fromName = process.env.MAILER_SENDGRID_FROM_NAME;
			}
			toEmail = config.toEmail;

			let from_email = new SendGrid.Email(fromEmail, fromName);
			let to_email = new SendGrid.Email(toEmail);
			let subject = config.subject || 'Mensaje de ASC Learning';

			let template, content, mail;

			if (config.plain) {
				// send multipart (text/plain + text/html) email
				template = handlebars.compile(fs.readFileSync(`${hbsTmplDir}/plain/${config.template}.txt`, 'utf8'));
				content = new SendGrid.Content('text/plain', template(data));
				mail = new SendGrid.Mail(from_email, subject, to_email, content);

				let templateHtml = handlebars.compile(fs.readFileSync(`${hbsTmplDir}/${config.template}.html`, 'utf8'));
				let contentHtml = new SendGrid.Content('text/html', templateHtml(data));
				mail.addContent(contentHtml);

			} else {
				// send text/html email
				template = handlebars.compile(fs.readFileSync(`${hbsTmplDir}/${config.template}.html`, 'utf8'));
				content = new SendGrid.Content('text/html', template(data));
				mail = new SendGrid.Mail(from_email, subject, to_email, content);
			}

			let request = sg.emptyRequest({
				method: 'POST',
				path: '/v3/mail/send',
				body: mail.toJSON(),
			});

			// untrack open_tracking
			request.body.tracking_settings = {
				'open_tracking': {
					'enable': false
				}
			};

			sg.API(request, function (error, response) {
				if (error) {
					logger.error(`SendGrid: Error sending e-mail to ${toEmail}`, error.toString());
					console.log('SENDING FAILED');
					reject(error);
				} else {
					hook.result = {'message': 'Correo enviado exitosamente'};
					logger.info(`SendGrid: E-mail sent to ${toEmail}`, response.body);
					console.log('EMAIL SENT');
					resolve(hook);
				}
			});
		});
	};
};
