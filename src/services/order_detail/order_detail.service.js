'use strict';

// Initializes the `order_detail` service on path `/order-detail`
const createService = require('feathers-sequelize');
const createModel = require('../../models/order_detail.model');
const hooks = require('./order_detail.hooks');
const filters = require('./order_detail.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		name: 'order-detail',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/order_detail', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('order_detail');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
