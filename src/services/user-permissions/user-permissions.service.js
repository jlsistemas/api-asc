'use strict';

// Initializes the `user_permissions` service on path `/user-permissions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-permissions.model');
const hooks = require('./user-permissions.hooks');
const filters = require('./user-permissions.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_permission_id',
		name: 'user-permissions',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/user-permissions', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-permissions');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
