'use strict';

// Initializes the `user_apps` service on path `/user_apps`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-apps.model');
const hooks = require('./user-apps.hooks');
const filters = require('./user-apps.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_app_id',
		name: 'user-apps',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/user-apps', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-apps');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
