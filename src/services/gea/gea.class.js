const errors = require('feathers-errors');

class Service {
	constructor (options) {
		this.options = options || {};
	}

	find (params) {
		return Promise.resolve([]);
	}

	get (id, params) {
		return new Promise((resolve, reject)=>{
			const services = params.services;
			services.orders.find({
				query: {
					'order_id_gea': {
						$gt: 0
					}
				}
			}).then((orders)=>{
				resolve(orders);
			}).catch((error)=>{
				reject(error);
			});
		});
	}

	create (data, params) {
		return new Promise((resolve, reject)=>{
			console.log('CREANDO ORDEN');
			let user_buyer;
			let country_id;
			let buyer_id;
			let currency_id;
			let price_list_id;
			let order_id;
			let result = {};
			let get_product_skus = [];
			let product_skus = [];
			let get_student_ids = [];
			let product_students;
			let get_product_level_price_ids = [];
			let product_levels;
			const services = params.services;
			const order = data;
			const buyer = data.buyer;
			const products = data.products;
			// otener country_id
			services.countries.find({query: {'code': 'mx'}}).then((country)=>{
				if (country.total == 0 || country.data.lentgh == 0 || country.data[0].country_id == null || country.data[0].country_id == undefined) {
					throw new errors.BadRequest('INVALID_COUNTRY');
				}
				country_id = country.data[0].country_id;
				// obtener o crear el buyer_id
				return services.users.find({query: {'id_gea': buyer.buyer_id_gea}});
			}).then((user)=>{
				if (user == null || user == undefined || user.total == 0 || user.data.length == 0) {
					return services.users.create({
						id_gea: buyer.buyer_id_gea,
						name: buyer.full_name,
						last_name: buyer.full_name,
						email: buyer.email
					});
				} else {
					user_buyer = user.data[0];
				}
			}).then((user)=>{
				if (user_buyer == undefined) {
					user_buyer = user;
				}
				buyer_id = user_buyer.user_id;
				// obtener la moneda
				return services.currencies.find({query: {'code': 'MXN'}});
			}).then((currency)=>{
				if (currency == null || currency == undefined || currency.total == 0 || currency.data.lentgh == 0) {
					throw new errors.BadRequest('INVALID_CURRENCY');
				}
				currency_id = currency.data[0].currency_id;
				// obtener la lista de precios
				return services.price_list.find({query: {'country_id': country_id, 'status': 'active'}});
			}).then((price_list)=>{
				if (price_list == null || price_list == undefined || price_list.total == 0 || price_list.data.lentgh == 0) {
					throw new errors.BadRequest('INVALID_PRICE_LIST');
				}
				price_list_id = price_list.data[0].price_list_id;
				order.country_id = country_id;
				order.buyer_id = buyer_id;
				order.currency_id = currency_id;
				order.price_list_id = price_list_id;
				// console.log(order);
				return services.orders.create(order);
			}).then((new_order)=>{
				order_id = new_order.order_id;
				// guardar los renglones
				products.map((product)=>{
					get_product_skus.push(services.products.find({query: {'sku': product.sku, 'status': 'active'}}));
				});
				return Promise.all(get_product_skus);
			}).then((skus)=>{
				product_skus = skus;
				products.map((product)=>{
					get_student_ids.push(services.students.find({query: {'id_gea': product.student.student_id_gea}}));
				});
				return Promise.all(get_student_ids);
			}).then((students)=>{
				product_students = students;
				product_skus.map((product)=>{
					if (product.total > 0 && product.data.length > 0) {
						let prod_id = product.data[0].product_id;
						get_product_level_price_ids.push(services.product_level_price.find({query: {'product_id': prod_id, 'price_list_id': price_list_id}}));
					}
				});
				return Promise.all(get_product_level_price_ids);
			}).then((product_levels)=>{
				let item_number = 1;
				let details = [];
				products.map((product)=>{
					let product_id = find_product_id(product.sku, product_skus);
					let product_level_price_id = find_level_price(product_id, product_levels);
					let name = find_product_name(product_id, product_skus);
					product.order_id = order_id;
					product.raw_data = JSON.parse(JSON.stringify(product));
					product.item_number = item_number;
					product.product_level_price_id = product_level_price_id;
					product.quantity = 1;
					product.description = name;
					details.push(services.order_detail.create(product));
					item_number++;
				});
				return Promise.all(details);
			}).then((result)=>{
				resolve(result);
			}).catch((error)=>{
				reject(error);
			});
		});
	}

	update (id, data, params) {
		return Promise.resolve(data);
	}

	patch (id, data, params) {
		return Promise.resolve(data);
	}

	remove (id, params) {
		return Promise.resolve({id});
	}
}

module.exports = function (options) {
	return new Service(options);
};

module.exports.Service = Service;

function find_product_id (sku, prod_skus) {
	let product_id;
	prod_skus.map((product)=>{
		if (product.data[0].sku == sku) {
			product_id = product.data[0].product_id;
		}
	});
	return product_id;
}

function find_level_price (product_id, prod_level) {
	let product_level_price_id;
	prod_level.map((product)=>{
		if (product.data[0].product_id == product_id) {
			product_level_price_id = product.data[0].product_level_price_id;
		}
	});
	return product_level_price_id;
}

function find_product_name (product_id, products) {
	let name;
	products.map((product)=>{
		if (product.data[0].product_id == product_id) {
			name = product.data[0].name;
		}
	});
	return name;
}
