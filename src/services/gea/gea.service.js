'use strict';

const errors = require('feathers-errors');
const validator = require('feathers-hooks-common');
const Ajv = require('ajv');
// Initializes the `gea` service on path `/gea`
const createService = require('./gea.class.js');
const hooks = require('./gea.hooks');
const filters = require('./gea.filters');

const schema = {
	'properties': {
		'order_id_gea': {
			'type': 'integer',
			'minLength': 1
		},
		'date': {
			'type': 'string',
			'format': 'date-time'
		},
		'total_amount': {
			'type': 'number',
			'minimum': 0.01
		},
		'currency': {
			'type': 'string',
			'minLength': 3,
			'maxLength': 3
		},
		'payment_method': {
			'type': 'string',
			'maxLength': 100
		},
		'reference': {
			'type': 'string',
			'maxLength': 100
		},
		'buyer': {
			'type': 'object',
			'properties': {
				'buyer_id_gea': {
					'type': 'integer',
					'minimum': 1
				},
				'full_name': {
					'type': 'string',
					'minLength': 4,
					'maxLength': 300
				},
				'email': {
					'type': 'string',
					'format': 'email'
				},
				'phone': {
					'type': 'string',
					'minLength': 8,
					'maxLength': 20
				}
			},
			'additionalProperties': false,
			'required': ['full_name', 'email']
		},
		'products': {
			'type': 'array',
			'minItems': 1,
			'items': {
				'type': 'object',
				'properties': {
					'sku': {
						'type': 'string',
						'minLength': 8,
						'maxLength': 9,
						'pattern': '((PE|SE){1}(P|S|0){1}[0-6]{1}[0-6]{1}1[0-1]{1}[0-9]{1})H?$'
					},
					'price': {
						'type': 'number',
						'minimum': 0.01
					},
					'currency': {
						'type': 'string',
						'minLength': 3,
						'maxLength': 3
					},
					'discount_percentage': {
						'type': 'number',
						'minimum': 0
					},
					'discount_code': {
						'type': 'string'
					},
					'student': {
						'type': 'object',
						'properties': {
							'student_id_gea': {
								'type': 'integer'
							},
							'school_id_gea': {
								'type': 'integer',
								'minimum': 1
							},
							'grade_id_gea': {
								'type': 'integer',
								'minimum': 1
							},
							'level_id_gea': {
								'type': 'integer',
								'minimum': 1
							},
							'name': {
								'type': 'string',
								'minLength': 1,
								'maxLength': 100
							},
							'last_name': {
								'type': 'string',
								'maxLength': 100
							},
							'second_last_name': {
								'type': 'string',
								'maxLength': 100
							},
							'email': {
								'type': 'string',
								'format': 'email'
							}
						},
						'anyOf': [
							{
								'properties': {
									'last_name': {
										'type': 'string',
										'minLength': 1
									}
								},
								'required': ['last_name']},
							{
								'properties': {
									'second_last_name': {
										'type': 'string',
										'minLength': 1
									}
								},
								'required': ['second_last_name']
							}
						],
						'required': ['school_id_gea', 'grade_id_gea', 'level_id_gea', 'name'],
						'additionalProperties': false
					}
				},
				'required': ['sku', 'price', 'currency', 'student']
			}
		}
	},
	'additionalProperties': false,
	'required': ['order_id_gea', 'date', 'total_amount', 'currency', 'buyer']
};

module.exports = function () {
	const app = this;
	const paginate = app.get('paginate');

	const options = {
		name: 'gea',
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/gea', createService(options));
	app.use('/gea/orders', createService(options));

	app.service('/gea/orders').hooks({
		before: {
			create: [
				(hook) => {
					console.log('API AUTHENTICATION');
					if (!hook.params || !hook.params.headers || !hook.params.headers['x-api-key']) {
						throw new errors.BadRequest('EMPTY_HEADERS');
					}
					if (hook.params.headers['x-api-key'] != 'HA0Nn[<Z{&QYa]zK_?}I-Sn2Fw]scR=0f2rT)Yd&PrWUvNNZGKV%zza6J2Iv}[]*G') {
						throw new errors.NotAuthenticated();
					}
				},
				validator.validateSchema(schema, Ajv, {
					'allErrors': true,
					'verbose': true,
					'format': 'full'
				}),
				(hook)=>{
					const services = hook.app.services;
					hook.params.services = services;
				}
			]
		}
	});
	// Get our initialized service so that we can register hooks and filters
	const service = app.service('gea');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
