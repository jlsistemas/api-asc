'use strict';

// Initializes the `user_role_countries` service on path `/user_role_countries`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-role-countries.model');
const hooks = require('./user-role-countries.hooks');
const filters = require('./user-role-countries.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_role_country_id',
		name: 'user-role-countries',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/user-role-countries', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-role-countries');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
