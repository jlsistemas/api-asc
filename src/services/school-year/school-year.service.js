'use strict';

// Initializes the `school_year` service on path `/school-year`
const createService = require('feathers-sequelize');
const createModel = require('../../models/school-year.model');
const hooks = require('./school-year.hooks');
const filters = require('./school-year.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		name: 'school-year',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/school-year', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('school-year');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
