'use strict';

// Initializes the `user_role_schools` service on path `/user_role_schools`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-role-schools.model');
const hooks = require('./user-role-schools.hooks');
const filters = require('./user-role-schools.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_role_school_id',
		name: 'user-role-schools',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/user-role-schools', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-role-schools');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
