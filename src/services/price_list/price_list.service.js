'use strict';

// Initializes the `price_list` service on path `/price-list`
const createService = require('feathers-sequelize');
const createModel = require('../../models/price_list.model');
const hooks = require('./price_list.hooks');
const filters = require('./price_list.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		name: 'price_list',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/price_list', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('price_list');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
