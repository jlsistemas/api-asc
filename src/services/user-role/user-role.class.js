class Service {
	constructor (options) {
		this.options = options || {};
	}

	get (id, params) {
		const sequelize = this.options.sequelize;
		const User = sequelize.models.users;
		return new Promise((resolve, reject)=>{
			return User.findOne({
				attributes: ['user_id', 'name', 'last_name', 'second_last_name', 'username', 'email'],
				where: {
					user_id: id,
					status: 'active'
				},
				include: [
					{
						model: sequelize.models.roles,
						attributes: ['name'],
						where: {
							status: 'active'
						}
					}
				]
			}).then((user)=>{
				resolve(user);
			}).catch((error)=>{
				console.log(error.toString());
				reject(error);
			});
		});
	}
}

module.exports = function (options) {
	return new Service(options);
};

module.exports.Service = Service;
