'use strict';

// Initializes the `user_role` service on path `/user_role`
const createService = require('./user-role.class.js');
const hooks = require('./user-role.hooks');
const filters = require('./user-role.filters');

module.exports = function () {
	const app = this;
	const paginate = app.get('paginate');
	const sequelize = app.get('sequelizeClient');

	const options = {
		id: 'user_role_id',
		name: 'user-role',
		paginate,
		sequelize
	};

	// Initialize our service with any options it requires
	app.use('/user-role', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-role');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
