'use strict';

// Initializes the `product_level_price` service on path `/product-level-price`
const createService = require('feathers-sequelize');
const createModel = require('../../models/product_level_price.model');
const hooks = require('./product_level_price.hooks');
const filters = require('./product_level_price.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		name: 'product_level_price',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/product_level_price', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('product_level_price');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
