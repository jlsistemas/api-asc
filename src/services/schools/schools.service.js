'use strict';

// Initializes the `schools` service on path `/schools`
const createService = require('feathers-sequelize');
const createModel = require('../../models/schools.model');
const hooks = require('./schools.hooks');
const filters = require('./schools.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'school_id',
		name: 'schools',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/schools', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('schools');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
