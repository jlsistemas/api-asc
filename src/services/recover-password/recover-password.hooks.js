'use strict';

const user = require('./hooks/user_find.js');
const token = require('./hooks/token_create.js');
const configure = require('./hooks/configure_email.js');
const email = require('../../hooks/sendEmail.js');
const {hashPassword} = require('feathers-authentication-local').hooks;
const {discard} = require('feathers-hooks-common');

module.exports = {
	before: {
		all: [],
		find: [],
		get: [
			token.verify()
		],
		create: [
			user.find(),
			token.create(),
			configure.email(),
			email.send()
		],
		update: [],
		patch: [
			hashPassword(),
			user.updatePassword()
		],
		remove: []
	},

	after: {
		all: [discard('password')],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};
