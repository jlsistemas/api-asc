'use strict';

// Initializes the `recover-password` service on path `/recover-password`
const createService = require('./recover-password.class.js');
const hooks = require('./recover-password.hooks');
const filters = require('./recover-password.filters');

module.exports = function () {
	const app = this;
	const paginate = app.get('paginate');

	const options = {
		name: 'recover-password',
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/recover-password', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('recover-password');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
