class Service {
	constructor (options) {
		this.options = options || {};
	}

	find (params) {
		return Promise.resolve([]);
	}

	get (id, params) {
		//
	}

	create (data, params) {
		//
	}

	update (id, data, params) {
		return Promise.resolve(data);
	}

	patch (id, data, params) {
		//
	}

	remove (id, params) {
		return Promise.resolve({id});
	}
}

module.exports = function (options) {
	return new Service(options);
};

module.exports.Service = Service;
