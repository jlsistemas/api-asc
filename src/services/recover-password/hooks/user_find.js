'use strict';

const errors = require('feathers-errors');

exports.find = function (options) {
	return function (hook) {
		let usernameFound, emailFound;
		return new Promise((resolve, reject)=>{
			console.log('RECOVER PASSWORD - FIND USER');
			getUsername(hook).then((result)=>{
				usernameFound = result;
				return getEmail(hook);
			}).then((result)=>{
				emailFound = result;
				if (emailFound == 'no-email' && usernameFound == 'no-username') {
					throw new errors.BadRequest('Es necesario proporcionar un nombre de usuario o correo electrónico');
				}
				if (emailFound == undefined && usernameFound == undefined) {
					throw new errors.NotFound('No encontrado');
				}
				if (usernameFound != undefined && usernameFound != 'no-username') {
					hook.result = usernameFound;
				}
				if (emailFound != undefined && emailFound != 'no-email') {
					hook.result = emailFound;
				}
				resolve(hook);
			}).catch((error)=>{
				reject(error);
			});
		});
	};
};

function getEmail (hook) {
	console.log('RECOVER PASSWORD - FIND BY EMAIL');
	const users = [];
	return new Promise((resolve, reject)=>{
		if (!hook.data.email || hook.data.email == undefined || hook.data.email == '') {
			resolve('no-email');
		}
		const sequelize = hook.app.get('sequelizeClient');
		const User = sequelize.models.users;
		return User.findAll({
			where: {
				email: hook.data.email
			}
		}).then((userFound)=>{
			if (!userFound) {
				throw new errors.NotFound('User not found');
			}
			userFound.map((user)=>{
				if (user.status == 'active') {
					users.push({'user_id': user.user_id});
				}
			});
			if (users.length < 1) {
				throw new errors.Forbidden('Access denied');
			}
			console.log('RECOVER PASSWORD - EMAIL FOUND');
			resolve (users);
		}).catch((error)=>{
			console.log('RECOVER PASSWORD - FIND BY EMAIL ERROR');
			reject (error);
		});
	});
}

function getUsername (hook) {
	console.log('RECOVER PASSWORD - FIND BY USERNAME');
	const users = [];
	return new Promise((resolve, reject)=>{
		if (!hook.data.username || hook.data.username == undefined || hook.data.username == '') {
			return (resolve('no-username'));
		}
		const sequelize = hook.app.get('sequelizeClient');
		const User = sequelize.models.users;
		return User.findOne({
			where: {
				username: hook.data.username
			}
		}).then((userFound)=>{
			if (!userFound) {
				throw new errors.NotFound('User not found');
			}
			if (userFound.status != 'active') {
				throw new errors.Forbidden('Access denied');
			}
			console.log('RECOVER PASSWORD - USERNAME FOUND');
			users.push({'user_id': userFound.user_id});
			resolve (users);
		}).catch((error)=>{
			console.log('RECOVER PASSWORD - FIND BY USERNAME ERROR');
			reject (error);
		});
	});
}

exports.updatePassword = function (options) {
	return function (hook) {
		console.log('RECOVER PASSWORD - UPDATE PASSWORD');
		let token, user;
		const tokens = hook.app.service('tokens');
		const users = hook.app.service('users');
		return new Promise((resolve, reject) => {
			let request_errors = {};
			let hash = hook.id;
			if (!hash || hash == undefined || hash == '') {
				request_errors.hash = 'Hash vacío o inválido';
			}
			let user_id = hook.data.user_id;
			if (!user_id || user_id == undefined || user_id == '') {
				request_errors.user_id = 'ID del usuario vacío o inválido';
			}
			let password = hook.data.password;
			if (!password || password == undefined || password == '') {
				request_errors.password = 'Contraseña vacío o inválida';
			}
			let confirm_password = hook.data.confirm_password;
			if (!confirm_password || confirm_password == undefined || confirm_password == '') {
				request_errors.confirm_password = 'La confirmación de la contraseña está vacía o inválida';
			}
			// if (password != confirm_password) {
			// 	request_errors.match = 'Las contraseñas no coinciden';
			// }
			if (Object.keys(request_errors).length > 0) {
				throw new errors.BadRequest('Errores de validación', {errors: request_errors});
			}
			return tokens.get(hash).then((token_found)=>{
				token = token_found;
				if (!token.user_id || token.user_id == '' || token.user_id == undefined) {
					throw new errors.Unprocessable('Usuario del token inválido');
				}
				if (token.user_id != user_id) {
					throw new errors.Unprocessable('Usuario del token no coincide');
				}
				if (token.used_at > new Date(-1)) {
					throw new errors.Unprocessable('Token usado');
				}
				if (new Date(token.expires_at) < new Date()) {
					throw new errors.Unprocessable('Token expirado');
				}
				return users.get(user_id);
			}).then((user_found)=>{
				user = user_found;
				if (user.status != 'active') {
					throw new errors.Unprocessable('Usuario no está activo');
				}
				return users.patch(user_id, {
					password: password
				});
			}).then((user)=>{
				return tokens.patch(token.hash, {
					used_at: new Date()
				});
			}).then((status)=>{
				console.log('RECOVER PASSWORD - PASSWORD UPDATED');
				hook.result = {
					'message': 'Contraseña actualizada'
				};
				resolve(hook);
			}).catch((error)=>{
				console.log('RECOVER PASSWORD - UPDATE PASSWORD ERROR');
				reject(error);
			});
		});
	};
};
