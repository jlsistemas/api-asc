'use strict';

const errors = require('feathers-errors');

exports.email = function (options) {
	return function (hook) {
		let hashes = [];
		let list = [];
		let toEmail, toName;
		return new Promise((resolve, reject)=>{
			console.log('RECOVER PASSWORD - EMAIL CONFIGURATION');
			const sequelize = hook.app.get('sequelizeClient');
			const token = sequelize.models.tokens;
			return token.findAll({
				where: {
					hash: {
						$in: hook.result
					}
				},
				include: [
					{
						model: sequelize.models.users
					}
				]
			}).then((users)=>{
				users.map((user)=>{
					toEmail = user.user.email;
					toName = (user.user.name.trim() + ' ' + user.user.last_name.trim() + ' ' + user.user.second_last_name.trim()).trim();
					list.push({
						'name': toName,
						'username': user.user.username,
						'hash': user.hash,
						'app_url': process.env.APP_URL
					});
				});
				let template = 'email-recover-password';
				let hashes = hook.result;
				if (hashes && Array.isArray(hashes) && hashes.length > 1) {
					template += '-multiple';
				}
				let config = {
					'data': {
						'toEmail': toEmail,
						'toName': toName,
						'list': list
					},
					'subject': 'Cambio de contraseña',
					'plain': false,
					'template': template,
					'toEmail': toEmail,
					'toName': toName
				};
				hook.result = config;
				console.log('RECOVER PASSWORD - EMAIL CONFIGURED');
				return (resolve(hook));
			}).catch((error)=>{
				console.log('RECOVER PASSWORD - EMAIL CONFIGURATION ERROR', error.toString());
				reject(error);
			});
		});
	};
};
