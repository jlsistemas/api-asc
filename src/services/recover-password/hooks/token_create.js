'use strict';

const errors = require('feathers-errors');

exports.create = function (options) {
	return function (hook) {
		let token_data = [];
		console.log('RECOVER PASSWORD - CREATE TOKEN');
		const sequelize = hook.app.get('sequelizeClient');
		const token = sequelize.models.tokens;
		return new Promise((resolve, reject)=>{
			let list = hook.result;
			return token.bulkCreate(
				list
			).then((tokens)=>{
				console.log('RECOVER PASSWORD - TOKEN(s) CREATED');
				tokens.map((token)=>{
					token_data.push(
						token.hash
					);
				});
				hook.result = token_data;
				return (resolve(hook));
			}).catch((error)=>{
				console.log('RECOVER PASSWORD - CREATE TOKEN ERROR', error.toString());
				reject(error);
			});
		});
	};
};

exports.verify = function (options) {
	return function (hook) {
		console.log('RECOVER PASSWORD - VERIFY TOKEN');
		let token, user;
		const tokens = hook.app.service('tokens');
		const users = hook.app.service('users');
		return new Promise((resolve, reject)=>{
			return tokens.get(hook.id).then((token_found)=>{
				token = token_found;
				if (!token.user_id || token.user_id == '' || token.user_id == undefined) {
					throw new errors.Unprocessable('Usuario del token inválido');
				}
				return users.get(token.user_id);
			}).then((user_found)=>{
				user = user_found;
				if (user.status != 'active') {
					throw new errors.Unprocessable('Usuario no está activo');
				}
				if (token.used_at > new Date(-1)) {
					throw new errors.Unprocessable('Token usado');
				}
				if (new Date(token.expires_at) < new Date()) {
					throw new errors.Unprocessable('Token expirado');
				}
				console.log('RECOVER PASSWORD - TOKEN VERIFIED');
				hook.result = {
					'message': 'Token válido',
					'hash': token.hash,
					'user_id': user.user_id
				};
				resolve(hook);
			}).catch((error)=>{
				console.log('RECOVER PASSWORD - VERIFY TOKEN ERROR', error.toString());
				reject(error);
			});
		});
	};
};
