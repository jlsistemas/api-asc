'use strict';

// Initializes the `users` service on path `/users`
const createService = require('feathers-sequelize');
const createModel = require('../../models/users.model');
const hooks = require('./users.hooks');
const filters = require('./users.filters');
const feathers_hooks = require('feathers-hooks-common');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_id',
		name: 'users',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/users', createService(options));
	app.use('/users/:user_id/roles', createService(options));
	app.use('/users/:user_id/permissions', createService(options));

	app.service('/users/:user_id/roles').before({
		find (hook) {
			console.log('GET USER ROLES');
			const sequelize = hook.app.get('sequelizeClient');
			hook.params.query.user_id = hook.params.user_id;
			hook.params.sequelize = {
				where: {
					user_id: hook.params.user_id,
					status: 'active'
				},
				include: [
					{
						model: sequelize.models.roles,
						through: {
							attributes: []
						},
						attributes: ['name'],
						where: {
							status: 'active'
						}
					}
				]
			};
		}
	});
	app.service('/users/:user_id/roles').after({
		find: feathers_hooks.discard('password')
	});

	app.service('/users/:user_id/permissions').before({
		find (hook) {
			// path y method se usan en combinación para determinar el permiso
			console.log('USER PERMISSIONS');
			const sequelize = hook.app.get('sequelizeClient');
			hook.params.query.user_id = hook.params.user_id;
			hook.params.sequelize = {
				attributes: {
					exclude: ['password']
				},
				where: {
					user_id: hook.params.user_id,
					status: 'active'
				},
				include: [
					{
						model: sequelize.models.roles,
						attributes: ['name'],
						through: {
							attributes: []
						},
						where: {
							status: 'active'
						},
						include: [
							{
								model: sequelize.models.permissions,
								attributes: ['name'],
								through: {
									attributes: []
								},
								where: {
									status: 'active'
								}
							}
						]
					}
				]
			};
		}
	});
	app.service('/users/:user_id/permissions').after({
		find: feathers_hooks.discard('password')
	});

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('users');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
