'use strict';

const errors = require('feathers-errors');

exports.length = function (options) {
	return function (hook) {
		return new Promise((resolve, reject)=>{
			console.log('VALIDATE PASSWORD - LENGTH');
			let min_length = process.env.PASSWORD_MIN_LENGTH || 12;
			if (!hook.data.password || hook.data.password == undefined || hook.data.password == '') {
				reject(new errors.BadRequest('Contraseña vacía o inválida'));
			}
			if (hook.data.password.length < min_length) {
				reject(new errors.Unprocessable('Contraseña debe tener mínimo ' + min_length + ' caracteres'));
			}
			hook.result = {
				'message': 'Contraseña válida'
			};
			resolve(hook);
		});
	};
};
