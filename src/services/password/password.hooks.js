'use strict';

const validate = require('./hooks/validate.js');

module.exports = {
	before: {
		all: [],
		find: [],
		get: [],
		create: [
			validate.length()
		],
		update: [],
		patch: [],
		remove: []
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};
