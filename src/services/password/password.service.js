'use strict';

// Initializes the `password` service on path `/password`
const createService = require('./password.class.js');
const hooks = require('./password.hooks');
const filters = require('./password.filters');

module.exports = function () {
	const app = this;
	const paginate = app.get('paginate');

	const options = {
		name: 'password',
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/password', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('password');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
