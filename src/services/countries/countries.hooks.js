'use strict';

const {authenticate} = require('feathers-authentication').hooks;
const globalHooks = require('../../hooks/checkRole');

module.exports = {
	before: {
		all: [
			// authenticate('jwt'),
			// globalHooks.checkRole()
		],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};
