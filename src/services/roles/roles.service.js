'use strict';

// Initializes the `roles` service on path `/roles`
const createService = require('feathers-sequelize');
const createModel = require('../../models/roles.model');
const hooks = require('./roles.hooks');
const filters = require('./roles.filters');
const feathers_hooks = require('feathers-hooks-common');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'role_id',
		name: 'roles',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/roles', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('roles');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
