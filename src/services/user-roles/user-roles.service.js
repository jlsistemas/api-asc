'use strict';

// Initializes the `user_roles` service on path `/user-roles`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-roles.model');
const hooks = require('./user-roles.hooks');
const filters = require('./user-roles.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'user_role_id',
		name: 'user-roles',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/user-roles', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('user-roles');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
