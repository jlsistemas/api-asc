'use strict';

// Initializes the `role_permissions` service on path `/role-permissions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/role-permissions.model');
const hooks = require('./role-permissions.hooks');
const filters = require('./role-permissions.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		id: 'role_permission_id',
		name: 'role-permissions',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/role-permissions', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('role-permissions');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
