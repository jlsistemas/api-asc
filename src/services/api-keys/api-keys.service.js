'use strict';

// Initializes the `api_keys` service on path `/api-keys`
const createService = require('feathers-sequelize');
const createModel = require('../../models/api-keys.model');
const hooks = require('./api-keys.hooks');
const filters = require('./api-keys.filters');

module.exports = function () {
	const app = this;
	const Model = createModel(app);
	const paginate = app.get('paginate');

	const options = {
		name: 'api-keys',
		Model,
		paginate
	};

	// Initialize our service with any options it requires
	app.use('/api-keys', createService(options));

	// Get our initialized service so that we can register hooks and filters
	const service = app.service('api-keys');

	service.hooks(hooks);

	if (service.filter) {
		service.filter(filters);
	}
};
